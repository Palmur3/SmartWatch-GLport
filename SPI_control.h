#ifndef SPI_CONTROL
#define SPI_CONTROL

#include "stm32l1xx.h"
#include "types_P.h"
#include "ReverseByte.h"

#define ALTER_FUNC_5 0x55555555

#define buff_t uint16_t

#define CLEAR_CMD 0x2000
#define WRITE_CMD 0x8000
#define TRAILER_CMD 0

#define DISP_HEIGH 128
#define DISP_LENGH 128

#define SPI_TRANSMIT_BUFF_TRAILER (2/sizeof(buff_t))

#define ROW_VIDEO_BUFF_SIZE DISP_HEIGH
#define COL_VIDEO_BUFF_SIZE (3*DISP_LENGH/8/sizeof(buff_t))

#define SPI_TRANSMIT_BUFF (COL_VIDEO_BUFF_SIZE * ROW_VIDEO_BUFF_SIZE)

#define SPI_BSY_TIMEOUT	0xFFFF

void InitSpi1(void);

//ErrorStateEnum SpiStartTransmit(volatile buff_t * ptr, uint32_t size);

void SpiTransmitInterruptRoutineBuff(void);
void SpiTransmitInterruptRoutineCmd(void);

ErrorStateEnum DisplayPrintBuff(volatile buff_t * ptr, uint8_t numberOfRow, uint8_t rowStart);
ErrorStateEnum SpiTransmitCmd(uint16_t cmd);

void (*CsEnable)(void);
void (*CsDisable)(void);

#endif
