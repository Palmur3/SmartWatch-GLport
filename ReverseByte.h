#ifndef REVERCE_BYTE
#define REVERCE_BYTE

#include "stm32l1xx.h"

uint8_t ReverseByte(uint8_t x);

#endif
