#ifndef PICO_GRAPH_LIB
#define PICO_GRAPH_LIB

#include "stm32l1xx.h"
#include "types_P.h"
#include "SPI_control.h"

//#define UG_COLOR uint8_t

typedef uint8_t      UG_U8;
typedef int8_t       UG_S8;
typedef uint16_t     UG_U16;
typedef int16_t      UG_S16;
typedef uint32_t     UG_U32;
typedef int32_t      UG_S32;
typedef uint8_t 	 UG_COLOR;

typedef enum
{
	FONT_TYPE_1BPP,
	FONT_TYPE_8BPP
} FONT_TYPE;

typedef struct
{
   unsigned char* p;
   FONT_TYPE font_type;
   UG_S16 char_width;
   UG_S16 char_height;
   UG_U16 start_char;
   UG_U16 end_char;
   UG_U8  *widths;
} UG_FONT;

#define USE_FONT_6X8

#ifdef USE_FONT_6X8
   extern const UG_FONT FONT_6X8;
#endif


#define COLOR_ERRASER	0x7
#define COLOR_BLACK		0x0
#define COLOR_WHITE		0x7
#define COLOR_RED		0x4
#define COLOR_GREEN		0x2
#define COLOR_BLUE		0x1
#define COLOR_YELLOW	0x6
#define COLOR_CYAN		0x3
#define COLOR_VIOLET	0x5

#define ARR_BLOCK_0 0
#define ARR_BLOCK_1 1
#define ARR_BLOCK_2 2

void UG_DrawLine( UG_S16 x1, UG_S16 y1, UG_S16 x2, UG_S16 y2, UG_COLOR c );

void DrawPixel(UG_S16 x, UG_S16 y, UG_COLOR c);

void _UG_PutChar( char chr, UG_S16 x, UG_S16 y, UG_COLOR fc, UG_COLOR bc, const UG_FONT* font);

#endif
