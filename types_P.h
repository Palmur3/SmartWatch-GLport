#ifndef ERROR_STATE_TYP
#define ERROR_STATE_TYP

typedef enum {ERROR_P = 0, SUCCESS_P = !ERROR_P} ErrorStateEnum;

#define FALSE_P	0
#define TRUE_P	!0

#define NULL_P	(void*) 0

#endif
