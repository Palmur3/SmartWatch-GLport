#include "LED_control.h"

void InputInit(void)
{
	RCC->AHBENR |= RCC_AHBENR_GPIOAEN;

	GPIOA->MODER &= ~GPIO_MODER_MODER3;	//input

	GPIOA->PUPDR &= ~GPIO_PUPDR_PUPDR3;	//pulldown
	GPIOA->PUPDR |= GPIO_PUPDR_PUPDR3_1;
}

uint32_t ReadPortA3(void)
{
	return (GPIOA->IDR & GPIO_IDR_IDR_3);
}

void LedInit(void)
{
	RCC->AHBENR |= RCC_AHBENR_GPIOAEN;

	GPIOA->MODER &= ~GPIO_MODER_MODER1;	//bug

	GPIOA->MODER &= 	~(GPIO_MODER_MODER0
						| GPIO_MODER_MODER1
						| GPIO_MODER_MODER2);
	GPIOA->MODER |=  	( GPIO_MODER_MODER0_0
						| GPIO_MODER_MODER1_0
						| GPIO_MODER_MODER2_0);
	GPIOA->OTYPER &= 	~(GPIO_OTYPER_IDR_0
						| GPIO_OTYPER_IDR_1
						| GPIO_OTYPER_IDR_2);
	GPIOA->OSPEEDR |= 	( GPIO_OSPEEDER_OSPEEDR0
						| GPIO_OSPEEDER_OSPEEDR1
						| GPIO_OSPEEDER_OSPEEDR2); //high frequency
	GPIOA->PUPDR &= 	~(GPIO_PUPDR_PUPDR0
						| GPIO_PUPDR_PUPDR1
						| GPIO_PUPDR_PUPDR2);
}

void LedOn_A0(void)
{
	GPIOA->BSRRL = GPIO_BSRR_BS_0;
}

void LedOff_A0(void)
{
	GPIOA->BSRRH = GPIO_BSRR_BS_0;
}

void LedOn_A1(void)
{
	GPIOA->BSRRL = GPIO_BSRR_BS_1;
}

void LedOff_A1(void)
{
	GPIOA->BSRRH = GPIO_BSRR_BS_1;
}

void LedOn_A2(void)
{
	GPIOA->BSRRL = GPIO_BSRR_BS_2;
}

void LedOff_A2(void)
{
	GPIOA->BSRRH = GPIO_BSRR_BS_2;
}

void inline LedChangeState_A0(void)
{
	GPIOA->ODR ^= GPIO_ODR_ODR_0;
}

void inline LedChangeState_A1(void)
{
	GPIOA->ODR ^= GPIO_ODR_ODR_1;
}

void inline LedChangeState_A2(void)
{
	GPIOA->ODR ^= GPIO_ODR_ODR_2;
}
