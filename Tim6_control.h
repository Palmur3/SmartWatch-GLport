#ifndef TIM_6_CONTROL
#define TIM_6_CONTROL

#include "stm32l1xx.h"
#include "types_P.h"

#include "LED_control.h"


void Tim6Init(void);

void Tim6InterruptRoutine(void);

#endif
