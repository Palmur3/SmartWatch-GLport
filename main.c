#include "stm32l1xx.h"
#include "types_P.h"
//#include "UART_control.h"
#include "LED_control.h"
#include "Tim6_control.h"
#include "SPI_control.h"
#include "ReverseByte.h"

#include "PicoGraphLib.h"

extern volatile union
{
	uint8_t volatile OneDimension_8[ROW_VIDEO_BUFF_SIZE * COL_VIDEO_BUFF_SIZE*2];
	uint8_t volatile TwoDimension_8[ROW_VIDEO_BUFF_SIZE][COL_VIDEO_BUFF_SIZE*2];
//	uint32_t volatile HalfDimension[ROW_VIDEO_BUFF_SIZE * COL_VIDEO_BUFF_SIZE / (sizeof(uint32_t)/sizeof(buff_t))];
	buff_t volatile OneDimension[ROW_VIDEO_BUFF_SIZE * COL_VIDEO_BUFF_SIZE];
	buff_t volatile TwoDimension[ROW_VIDEO_BUFF_SIZE][COL_VIDEO_BUFF_SIZE];
} VideoBuff;

void NopDelay(volatile uint32_t i)
{
	for(; i > 1; i --)
		__NOP();
}

void BuffInit(void) //3072
{
	uint32_t i;
	for(i = 0; i < SPI_TRANSMIT_BUFF; i++)
		VideoBuff.OneDimension[i] = 0x0000;
}


int main(void)
{
	LedInit();
	InputInit();

	InitSpi1();
	BuffInit();

	//set cs low
	LedOff_A1();
	LedOn_A2();

	Tim6Init();

	//CS callbacks
	CsEnable = LedOn_A1;
	CsDisable = LedOff_A1;

	SpiTransmitCmd(CLEAR_CMD);		//TODO: FIX: ��� ���� ������� �� ����������� ���������� ����������

	NopDelay(4000);

	//draw example
	DrawExamplePicture();

    while(1) {
    }
}

void DrawExamplePicture(void)
{
	UG_DrawLine(0, 25, 127, 25, COLOR_YELLOW);
	UG_DrawLine(0, 30, 127, 30, COLOR_VIOLET);
	UG_DrawLine(0, 35, 127, 35, COLOR_CYAN);

	_UG_PutChar('P', 14, 70, COLOR_WHITE, COLOR_BLACK, &FONT_6X8);
	_UG_PutChar('A', 23, 70, COLOR_WHITE, COLOR_BLACK, &FONT_6X8);
	_UG_PutChar('L', 32, 70, COLOR_WHITE, COLOR_BLACK, &FONT_6X8);
	_UG_PutChar('T', 41, 70, COLOR_WHITE, COLOR_BLACK, &FONT_6X8);
	_UG_PutChar('S', 50, 70, COLOR_WHITE, COLOR_BLACK, &FONT_6X8);
	_UG_PutChar('E', 59, 70, COLOR_WHITE, COLOR_BLACK, &FONT_6X8);
	_UG_PutChar('V', 68, 70, COLOR_WHITE, COLOR_BLACK, &FONT_6X8);

	_UG_PutChar('E', 14, 80, COLOR_WHITE, COLOR_BLACK, &FONT_6X8);
	_UG_PutChar('U', 23, 80, COLOR_WHITE, COLOR_BLACK, &FONT_6X8);
	_UG_PutChar('G', 32, 80, COLOR_WHITE, COLOR_BLACK, &FONT_6X8);
	_UG_PutChar('E', 41, 80, COLOR_WHITE, COLOR_BLACK, &FONT_6X8);
	_UG_PutChar('N', 50, 80, COLOR_WHITE, COLOR_BLACK, &FONT_6X8);
	_UG_PutChar('I', 59, 80, COLOR_WHITE, COLOR_BLACK, &FONT_6X8);
	_UG_PutChar('Y', 68, 80, COLOR_WHITE, COLOR_BLACK, &FONT_6X8);

	_UG_PutChar('P', 14, 90, COLOR_GREEN, COLOR_BLACK, &FONT_6X8);
	_UG_PutChar('3', 23, 90, COLOR_GREEN, COLOR_BLACK, &FONT_6X8);
	_UG_PutChar('4', 32, 90, COLOR_GREEN, COLOR_BLACK, &FONT_6X8);
	_UG_PutChar('0', 41, 90, COLOR_GREEN, COLOR_BLACK, &FONT_6X8);
	_UG_PutChar('1', 50, 90, COLOR_GREEN, COLOR_BLACK, &FONT_6X8);

	_UG_PutChar('I', 5,  110, COLOR_BLUE, COLOR_BLACK, &FONT_6X8);
	_UG_PutChar('T', 14, 110, COLOR_BLUE, COLOR_BLACK, &FONT_6X8);
	_UG_PutChar('M', 23, 110, COLOR_BLUE, COLOR_BLACK, &FONT_6X8);
	_UG_PutChar('O', 32, 110, COLOR_BLUE, COLOR_BLACK, &FONT_6X8);
	_UG_PutChar(',', 41, 110, COLOR_BLUE, COLOR_BLACK, &FONT_6X8);
	_UG_PutChar(' ', 50, 110, COLOR_BLUE, COLOR_BLACK, &FONT_6X8);
	_UG_PutChar('2', 59, 110, COLOR_BLUE, COLOR_BLACK, &FONT_6X8);
	_UG_PutChar('0', 68, 110, COLOR_BLUE, COLOR_BLACK, &FONT_6X8);
	_UG_PutChar('1', 77, 110, COLOR_BLUE, COLOR_BLACK, &FONT_6X8);
	_UG_PutChar('6', 86, 110, COLOR_BLUE, COLOR_BLACK, &FONT_6X8);

	DisplayPrintBuff(NULL_P, 128, 0);
}
